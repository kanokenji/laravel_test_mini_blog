<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ArticlesRepository;
use App\Article;
use App\User;
use Auth;

class ArticleController extends SiteController
{
    public function __construct(ArticlesRepository $article_rep)
    {
        parent::__construct();
        $this->template = 'index';
        $this->article_rep = $article_rep;
    }

    public function getArticles($select = '*', $search = FALSE)
    {
        $articles = $this->article_rep->get($select, $search);
        return $articles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_input = $request->has('search')?$request->input('search'):null;
        $this->title = "Main";
        if ($search_input) {
            $articles = $this->getArticles("*",[$search_input,['title','text']]);
        } else {
            $articles = $this->getArticles();
        }
        $data = [];
        $data ['title'] = $this->title;
        $data ['articles'] = $articles;
        $data ['auth_user'] = Auth::user();
        $this->content = view('articles.index', $data)->render();
        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = "Create article";
        $data = [];
        $data ['title'] = $this->title;
        $data ['authors'] = User::select('*')->where('id', '<>', Auth::user()->id)->get();
        $this->content = view('articles.create', $data)->render();
        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'title' => 'required',
        ]);
        $result = $this->article_rep->addArticle($request);
        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }
        return redirect()->route('home')->with($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $this->title = "Article";
        $data = [];
        $data ['article'] = $article;
        $data ['title'] = $this->title;
        $data ['auth_user'] = Auth::user();
        $this->content = view('articles.show', $data)->render();
        return $this->renderOutput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        if(!Auth::user() || !$article->isAuthor(Auth::user())) {
            return abort('404');
        }
        $this->title = "Edit article";
        $authors_sel = array();
        $i_key = 0;
        foreach ($article->users as $author_article) {
            $authors_sel[$i_key] = $author_article->id;
            $i_key++;
        }
        $data = [];
        $data ['title'] = $this->title;
        $data ['article'] = $article;
        $data ['authors'] = User::select('*')->where('id', '<>', Auth::user()->id)->get();
        $data ['authors_sel'] = $authors_sel;
        $this->content = view('articles.edit', $data)->render();
        return $this->renderOutput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validate($request, [
            'text' => 'required',
            'title' => 'required',
        ]);
        $result = $this->article_rep->updateArticle($request, $article);
        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }

        return redirect(route('home'))->with($result);
    }

    public function restore_article(Request $request, $article_id)
    {
        if(!Auth::user()) {
            return abort('404');
        }
        $result = $this->article_rep->restoreArticle($request, $article_id);
        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }

        return redirect(route('trash'))->with($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        if(!Auth::user() || !$article->isAuthor(Auth::user())) {
            return abort('404');
        }
        $result = $this->article_rep->deleteArticle($article);

        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }

        return redirect(route('home'))->with($result);
    }

    public function confirm_destroy(Request $request, $article_id)
    {
        if(!Auth::user()) {
            return abort('404');
        }
        $result = $this->article_rep->confirm_deleteArticle($article_id);

        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }

        return redirect(route('trash'))->with($result);
    }
}
