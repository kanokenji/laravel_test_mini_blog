<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UsersRepository;
use App\User;
use Auth;
use Validator;
use Hash;

class UserController extends SiteController
{
    public function __construct(UsersRepository $user_rep) {
        parent::__construct();
        $this->template = 'index';
        $this->user_rep = $user_rep;
    }

    public function profile(Request $request)
    {
        $this->title = "My profile";
        $data = [];
        $data ['title'] = $this->title;
        $data ['auth_user'] = Auth::user();
        $this->content = view('auth.profile', $data)->render();
        return $this->renderOutput();
    }

    public function edit()
    {
        $this->title = "Edit profile";
        $data = [];
        $data ['title'] = $this->title;
        $data ['auth_user'] = Auth::user();
        $this->content = view('auth.edit_user', $data)->render();
        return $this->renderOutput();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'nickname' => 'required|unique:users,nickname,'.Auth::user()->id,
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'sex' => 'required',
            'filename' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $result = $this->user_rep->updateUser($request, Auth::user());

        if(is_array($result) && !empty($result['error'])) {
            return redirect()->back()->withInput()->with($result);
        }

        return redirect()->route('profile')->with($result);
    }

    public function edit_password()
    {
        $this->title = "Edit passwords";
        $data = [];
        $data ['title'] = $this->title;
        $data ['user'] = Auth::user();
        $this->content = view('auth.edit_password', $data)->render();
        return $this->renderOutput();
    }

    public function update_password(Request $request)
    {
        $messages = [
            'current-passwords.required' => 'Please enter current passwords',
            'passwords.required' => 'Please enter passwords',
        ];

        $validator = $this->validate($request, [
            'current-passwords' => 'required',
            'passwords' => 'required|same:passwords|string|min:6',
            'password_confirmation' => 'required|same:passwords',
        ]);
        $current_password = Auth::user()->password;
        if(Hash::check($request['current-passwords'], $current_password))
        {
            $result = $this->user_rep->updateUserPassword($request, Auth::user());

            if(is_array($result) && !empty($result['error'])) {
                return redirect()->back()->withInput()->with($result);
            }
        }

        return redirect()->route('profile')->with($result);
    }

    public function get_experience(Request $request)
    {
        $result = $this->user_rep->getUserExperience($request);
        return response()->json($result);
    }

    public function set_experience(Request $request)
    {
        $result = $this->user_rep->setUserExperience($request);
        return response()->json($result);
    }
}
