<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SiteController extends Controller
{
    protected $article_rep = FALSE;

    protected $user_rep = FALSE;

    protected $content = FALSE;

    protected $authors_articles = FALSE;

    protected $title;

    protected $template;

    protected $vars = array();

    public function __construct() {}

    protected function renderOutput()
    {
        $this->vars = array_add($this->vars,'title',$this->title);
        if($this->content) {
            $this->vars = array_add($this->vars,'content',$this->content);
        }
        $authors_articles = User::select()->get();
        $data = [];
        $data ['authors_articles'] = $authors_articles;
        $this->right_sidebar = view('right_sidebar', $data)->render();
        if($this->right_sidebar) {
            $this->vars = array_add($this->vars,'right_sidebar',$this->right_sidebar);
        }
        return view($this->template)->with($this->vars);
    }
}
