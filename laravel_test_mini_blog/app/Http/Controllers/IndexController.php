<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class IndexController extends SiteController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = 'index';
    }

    public function author(Request $request, User $user)
    {
        $this->title = "Author's articles";
        $data = [];
        $data ['title'] = $this->title;
        $data ['user'] = $user;
        $data ['auth_user'] = Auth::user();
        $this->content = view('user_articles', $data)->render();
        return $this->renderOutput();
    }

    public function trash()
    {
        $this->title = "My trash";
        $articles_trash_user = Auth::user()->articles()->onlyTrashed()->get();

        $data = [];
        $data ['title'] = $this->title;
        $data ['articles'] = $articles_trash_user;
        $data ['auth_user'] = Auth::user();
        $this->content = view('trash', $data)->render();
        return $this->renderOutput();
    }

    public function about()
    {
        $this->title = "About project";

        $data = [];
        $data ['title'] = $this->title;
        $this->content = view('about', $data)->render();
        return $this->renderOutput();
    }
}
