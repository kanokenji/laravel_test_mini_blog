<?php

namespace App\Repositories;

use App\Article;
use Auth;

class ArticlesRepository extends Repository
{

    public function __construct(Article $article)
    {
        $this->model = $article;
    }

    public function addArticle($request)
    {
        $data = $request->except('_token');
        if(empty($data)) {
            return array('error'=>'No data');
        }
        $this->model->fill($data);
        if($request->user()->articles()->save($this->model)) {
            if (array_key_exists('authors', $data )) {
                $this->model->users()->sync($data['authors'],false);
            }
            return array('status'=>'Article has been added successfully');
        }
    }

    public function updateArticle($request, $article)
    {
        $data = $request->except(['_token','_method']);
        if(empty($data) || empty($article)) {
            return array('error'=>'No data');
        }
        $article->fill($data);

        if($article->update()) {
            $article->users()->sync(Auth::user()->id);
            if (array_key_exists('authors', $data )) {
                $article->users()->sync($data['authors'],false);
            }
            return array('status'=>'Article has been changed successfully');
        }
    }

    public function deleteArticle($article) {
        if($article->delete()) {
            return array('status'=>'Article has been deleted successfully');
        }
    }

    public function confirm_deleteArticle($article_id)
    {
        $article_for_delete = Article::withTrashed()->find($article_id);
        $article_for_delete->users()->detach();
        if($article_for_delete->forceDelete()) {
            return array('status'=>'Article has been deleted forever successfully');
        }
    }

    public function restoreArticle($request, $article_id)
    {
        $article_for_restore = Article::withTrashed()->find($article_id);
        if($article_for_restore->restore()) {
            return array('status'=>'Article has been restored successfully');
        }
    }

}
