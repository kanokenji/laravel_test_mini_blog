<?php

namespace App\Repositories;

abstract class Repository
{
    protected $model = FALSE;

    public function get($select = '*', $search = FALSE)
    {
        $builder = $this->model->select($select);
        if ($search) {

            $what_search = $search[0];
            $where_search = $search[1];
            foreach ($where_search as $key => $column) {
                if ($key==0) {
                    $builder = $builder->where($column, 'LIKE', '%' . $what_search . '%');
                } else {
                    $builder = $builder->orWhere($column, 'LIKE', '%' . $what_search . '%');
                }
            }
        }
        return $this->check($builder->get());
    }

    protected function check($result)
    {
        if($result->isEmpty()) {
            return FALSE;
        }
        return $result;
    }

}
