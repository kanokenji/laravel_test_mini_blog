<?php

namespace App\Repositories;

use App\User;
use Hash;
use Image;
use File;

class UsersRepository extends Repository
{
    public $stopAsync = true;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function updateUser($request, $user)
    {
        $data = $request->except('_token');
        $clear_avatar = false;
        if (array_key_exists("clear_avatar",$data)) {
            $clear_avatar = $data['clear_avatar']=='on'?true:false;
            $data = $request->except('clear_avatar');
        }
        if ($clear_avatar && $user->avatar) {
            $data['avatar'] = '';
            $pathToImage = public_path().'/images/portfolio/'.$user->avatar;
            File::delete($pathToImage);
        }
        if (!$clear_avatar && array_key_exists("filename",$data) && $data['filename'] ) {
            $originalImage= $request->file('filename');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/images/portfolio/';
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName());
            $data['avatar'] = time().$originalImage->getClientOriginalName();
        }
        if(empty($data) || empty($user)) {
            return array('error'=>'No data');
        }
        if (array_key_exists("show_phone",$data)) {
            $data['show_phone'] = $data['show_phone']=='on'?1:0;
        }
        else {
            $data['show_phone'] = 0;
        }
        $user->fill($data);
        if($user->update()) {
            return array('status'=>'Information has been updated');
        }
    }

    public function updateUserPassword($request, $user)
    {
        $user->password = Hash::make($request['passwords']);
        if($user->save()) {
            return array('status'=>'Password has been changed');
        }
    }

    public function getUserExperience($request)
    {
        if(empty($request)) {
            return array('error'=>'No user id');
        }
        $user_experience = $this->getExperience($request->user_id);
        return array('success'=>'Information about experience user','user_experience' => $user_experience);
    }

    public function getExperience($user_id)
    {
        $user = User::find($user_id);
        return $user->experience;
    }

    public function setUserExperience($request)
    {
        if(empty($request)) {
            return array('error'=>'No user id');
        }
        $result = $this->setExperience($request->user_id);
        return array('success'=>$result);

    }

    public function setExperience($user_id)
    {
        $user = User::find($user_id);
        $user->experience = random_int(1, 50);

        if ($user->update())
            return array('success'=>'Information has been set experience user');
        return array('error'=>'Information has not set experience user');
    }

}
