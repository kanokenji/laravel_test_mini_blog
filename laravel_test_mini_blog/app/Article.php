<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Article extends Model
{
    use SoftDeletes; //trait

    protected $dates=['deleted_at']; //current pointer's time

    protected $fillable = [
        'id', 'text', 'title', 'created_at'
    ];

    public function users() {
        return $this->belongsToMany('App\User','article_user')->withTimestamps();
    }

    public function isAuthor($user)
    {
        if ($user) {
            if($this->trashed()) {
                return null;
            }
            if ($this->users()->find($user->id)) {
                return true;
            } else {
                return false;
            }
        }
        return'mismatch';
    }
}
