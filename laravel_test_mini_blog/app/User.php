<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'name', 'email', 'passwords', 'nickname', 'surname', 'avatar', 'phone', 'sex', 'show_phone', 'experience'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'passwords', 'remember_token',
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Article','article_user')->withTimestamps();
    }
}
