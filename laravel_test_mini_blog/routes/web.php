<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();
Route::group(['middleware' => ['web']], function () {

    Route::get('/', ['uses'=>'ArticleController@index', 'as'=>'home']);
    Route::get('/{author}/articles', ['uses'=>'IndexController@author', 'as'=>'author']);
    Route::get('/about', ['uses'=>'IndexController@about', 'as'=>'about']);

    Route::group(['middleware' => ['auth']], function() {

        //-------Trash--------
        Route::get('articles/trash', ['uses'=>'IndexController@trash', 'as'=>'trash']);
        Route::patch('articles/trash/{article_id}', ['uses'=>'ArticleController@restore_article', 'as'=>'restore_article']);
        Route::delete('article/confirm_destroy/{article_id}', ['uses'=>'ArticleController@confirm_destroy', 'as'=>'article.confirm_destroy']);
        //--------------------
        Route::resource('article','ArticleController', ['except' => ['index','show']]);
        //----User profile----
        Route::get('/profile', ['uses'=>'UserController@profile', 'as'=>'profile']);
        Route::get('/profile/edit', ['uses'=>'UserController@edit', 'as'=>'edit_user']);
        Route::post('/profile/edit', ['uses'=>'UserController@update', 'as'=>'update_user']);
        Route::get('/profile/passwords', ['uses'=>'UserController@edit_password', 'as'=>'edit_password']);
        Route::post('/profile/passwords', ['uses'=>'UserController@update_password', 'as'=>'update_password']);
        //--------------------
        Route::post('/get_experience','UserController@get_experience');
        Route::post('/set_experience','UserController@set_experience');
    });
    Route::get('/article/{article}', ['uses'=>'ArticleController@show', 'as'=>'article.show']);
});

