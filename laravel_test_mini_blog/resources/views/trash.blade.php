<div class="main-content">
    <article>
        <h2 class="page-title">{{ $title }}</h2>
        <p>
            <a href="{{ route('home') }}" class="btn btn-default btn-sm btn-back" type="button">Return main</a>
        </p>
        @if ($articles)
            @foreach ($articles as $article)
                <a href="{{ route('article.show', $article->id) }}"><h2 class="post-title">{{ $article->title }}</h2></a>
                <div class="post-meta">
                    @foreach ($article->users as $user)
                        <span><a href="{{ route('author',$user->id) }}"><i class="fa fa-user post-meta-icon"></i> {{ $user->name }}</a></span>
                    @endforeach
                    <span><i class="fa fa-calendar-check-o post-meta-icon"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('F d, Y')}} </span>
                </div>
                <div class="post-content">
                    <p>{{{ str_limit($article->text, $limit = 250, $end = '...') }}}</p>

                    @if ($auth_user && $article->isAuthor($auth_user)==null)
                        <form class="input-group pull-left" method="POST" action="{{ route('restore_article', $article->id) }}">
                            {{ csrf_field() }}
                            {!! method_field('patch') !!}
                            <button type="submit" class="btn btn-default btn-sm btn-category">Restore</button>
                        </form>
                        <form class="input-group pull-left" method="POST" action="{{ route('article.confirm_destroy', $article->id) }}">
                            {{ csrf_field() }}
                            {!! method_field('delete') !!}&nbsp;
                            <button type="submit" class="btn btn-default btn-sm btn-back">Confirm deletion</button>
                        </form>
                    @endif
                </div>
            @endforeach
        @else
            <div class="post-content">
                <p>No article...</p>
            </div>
        @endif
    </article>
</div><!-- main-content -->

<!-- <div class="pagination">
  <nav>
    <ul class="pagination">
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">Next</a></li>
    </ul>
  </nav>
</div> -->
