<div class="main-content">
    <article>
        <h2 class="page-title">{{ $title }}</h2>
        <p>
            <a href="{{ route('home') }}" class="btn btn-default btn-sm btn-back" type="button">Return main</a>
        </p>
        <div class="form-body">
            <form class="form-horizontal" method="POST" action="{{ route('article.store') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title..." value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="text">Text</label>
                    <textarea class="form-control" name="text" placeholder="Text..." rows="3">{{ old('text') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="sel1">The authors:</label>
                    <select class="selectpicker form-control" id="authors" name="authors[]" data-live-search="true" title="Select authors" data-hide-disabled="true" data-actions-box="true" multiple>
                        @if($authors)
                            @foreach ($authors as $author)
                                <option value="{{ $author->id }}"
                                        @if(old('authors') && in_array($author->id, old('authors')))
                                        selected="selected"
                                    @endif
                                >{{ $author->nickname }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <button type="submit" class="btn btn-success form-btn">Submit</button>
            </form>
        </div>
    </article>
</div><!-- main-content -->
