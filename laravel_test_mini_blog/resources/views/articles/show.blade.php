<div class="main-content">
    <article>
        <h2 class="page-title">{{ $article->title }}</h2>
        <p>
            <a href="{{ route('home') }}" class="btn btn-default btn-sm btn-back" type="button">Return main</a>
        </p>
        @if ($article)
            <div class="post-meta">
                @foreach ($article->users as $user)
                    <span><a href="{{ route('author',$user->id) }}"><i class="fa fa-user post-meta-icon"></i> {{ $user->nickname }}</a></span>
                @endforeach
                <span><i class="fa fa-calendar-check-o post-meta-icon"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('F d, Y')}} </span>
            </div>
            <div class="post-content">
                <p class="post">{{ $article->text }}</p>
                @if ($auth_user && $article->isAuthor($auth_user))
                    <form class="form-inline" method="POST" action="{{ route('article.destroy', $article->id) }}">
                        <a href="{{ route('article.edit',$article->id) }}" class="btn btn-default btn-sm btn-category">Edit</a>
                        {{ csrf_field() }}
                        {!! method_field('delete') !!}
                        <button type="submit" class="btn btn-default btn-sm btn-back">Delete</button>
                    </form>
                @endif
            </div>
        @else
            <div class="post-content">
                <p>No articles...</p>
            </div>
        @endif
    </article>
</div><!-- main-content -->
