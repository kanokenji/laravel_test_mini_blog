<div class="right-sidebar">
    <div class="righ-sidebar-body">

        <div class="item">
            <h4 class="post-title slide-title">Authors of articles</h4>
            <div class="post-meta">
                @if($authors_articles)
                    @foreach ($authors_articles as $author)
                        <a href="{{ route('author',$author->id) }}" class="btn btn-default tag" role="button">{{ $author->nickname }}</a>
                    @endforeach
                @endif
            </div>
        </div>

    </div><!-- Righ-sidebar-body -->
</div><!-- Right-Sidebar -->
