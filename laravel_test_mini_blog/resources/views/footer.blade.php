<footer>
    <div class="footer-menu">
        <div class="container">
            <div class="col-md-12 col-sm-12 center-block">
                <h4 class="footer-head">This is a test blog</h4>
            </div>
        </div>
        <div id="toTop" class="btn btn-info" style="display: block;">
            <span class="fa fa-angle-up"></span>
        </div><!-- /Go-to-top -->
    </div><!-- Footer-menu -->
</footer>
