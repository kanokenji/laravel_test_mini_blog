<div class="main-content">
    <article>
        <h2 class="page-title">{{ $title }}: {{ $user->name }}</h2>
        <p>
            <a href="{{ route('home') }}" class="btn btn-default btn-sm btn-back" type="button">Return main</a>
        </p>
    </article>

    @if ($user->articles)
        @foreach ($user->articles as $article)
            <article>
                <a href="{{ route('article.show', $article->id) }}"><h2 class="post-title">{{ $article->title }}</h2></a>
                <div class="post-meta">
                    @foreach ($article->users as $user)
                        <span><a href="#"><i class="fa fa-user post-meta-icon"></i> {{ $user->name }}</a></span>
                    @endforeach
                    <span><i class="fa fa-calendar-check-o post-meta-icon"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('F d, Y')}} </span>
                </div>
                <div class="post-content">
                    <p>{{{ str_limit($article->text, $limit = 250, $end = '...') }}}</p>
                    @if ($auth_user && $article->isAuthor($auth_user))
                        <form class="form-inline" method="POST" action="{{ route('article.destroy', $article->id) }}">
                            <a href="{{ route('article.edit',$article->id) }}" class="btn btn-default btn-sm btn-category">Edit</a>
                            {{ csrf_field() }}
                            {!! method_field('delete') !!}
                            <button type="submit" class="btn btn-default btn-sm btn-back">Delete</button>
                        </form>
                    @endif
                </div>
            </article>
        @endforeach
    @else
        <article>
            <div class="post-content">
                <p>No articles...</p>
            </div>
        </article>
    @endif
</div><!-- main-content -->

<!-- <div class="pagination">
  <nav>
    <ul class="pagination">
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">Next</a></li>
    </ul>
  </nav>
</div> -->
